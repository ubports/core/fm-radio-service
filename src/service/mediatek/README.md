The fmr directory is taken verbatim from

  https://android.googlesource.com/platform/packages/apps/FMRadio/+/refs/heads/master/jni/fmr/

The `AudDrv*` files are taken from the [BQ repository for the Aquaris
E4.5](https://github.com/bq/aquaris-E4.5), where they are located at
`mediatek/platform/mt6582/kernel/drivers/sound/`.
